﻿using System.Collections.Generic;
using System.IO;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PcapConverter;

namespace PcapConverterTests
{
    [TestClass]
    public class PcapToCsvTests
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PcapToCsvTests));

        [TestMethod]
        public void when_file_path_is_null()
        {
            log.Info("when_file_path_is_null");
            string actual = PcapHandler.ConvertPcapToCsvFile(null);
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void when_file_path_is_empty()
        {
            log.Info("when_file_path_is_empty");
            string actual = PcapHandler.ConvertPcapToCsvFile(string.Empty);
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void when_file_is_valid()
        {
            log.Info("when_file_is_valid");
            string filePath = @"..\..\..\SampleCaptures\ChangeIPUsingDCP.pcap";
            string actual = PcapHandler.ConvertPcapToCsvFile(filePath);
            Assert.IsTrue(File.Exists(actual));
        }

        [TestMethod]
        public void when_file_has_14xx_packet()
        {
            log.Info("when_file_has_14xx_packet");
            string filePath = @"..\..\..\SampleCaptures\ioc_iod1_2_mrp_rtc3_with_cr.pcap";
            string actual = PcapHandler.ConvertPcapToCsvFile(filePath);
            Assert.IsTrue(File.Exists(actual));
        }

        [TestMethod]
        public void when_file_is_invalid()
        {
            log.Info("when_file_is_invalid");
            string filePath = @"..\..\..\SampleCaptures\ChangeIPUsingDCP_auto.csv";
            string actual = PcapHandler.ConvertPcapToCsvFile(filePath);
            Assert.IsFalse(File.Exists(actual));
        }

        [TestMethod]
        public void with_udp_filter()
        {
            log.Info("with_udp_filter");
            string filePath = @"..\..\..\SampleCaptures\ioc_iod1_2_mrp_rtc3_with_cr.pcap";
            string actual = PcapHandler.ConvertPcapToCsvFile(filePath, PcapFilter.s_Udp);
            Assert.IsTrue(File.Exists(actual));
        }

        [TestMethod]
        public void with_pn_dcp_filter()
        {
            log.Info("with_pn_dcp_filter");
            string filePath = @"..\..\..\SampleCaptures\ChangeIPUsingDCP.pcap";
            string actual = PcapHandler.ConvertPcapToCsvFile(filePath, PcapFilter.s_Pn_Dcp);
            Assert.IsTrue(File.Exists(actual));
        }

        [TestMethod]
        public void when_number_of_packets_are_correct()
        {
            log.Info("when_number_of_packets_are_correct");
            string filePath = @"..\..\..\SampleCaptures\ChangeIPUsingDCP.pcap";
            string csvFile = PcapHandler.ConvertPcapToCsvFile(filePath);
            List<CsvPacket> csvPackets = CsvReader.Read(csvFile);
            int actual = 6;
            int expected = csvPackets.Count;
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void when_number_of_packets_are_correct_with_pn_dcp_filter()
        {
            log.Info("when_number_of_packets_are_correct_with_pn_dcp_filter");
            string filePath = @"..\..\..\SampleCaptures\ChangeIPUsingDCP.pcap";
            string csvFile = PcapHandler.ConvertPcapToCsvFile(filePath, PcapFilter.s_Pn_Dcp);
            List<CsvPacket> csvPackets = CsvReader.Read(csvFile);
            int actual = 4;
            int expected = csvPackets.Count;
            Assert.AreEqual(actual, expected);
        }
    }
}
