﻿using System.Diagnostics;
using System.IO;

namespace PcapConverter
{
    public class PcapHandler
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Wireshark Directory
        /// </summary>
        private const string s_WiresharkDirectory = @"C:\Program Files\Wireshark";

        /// <summary>
        /// tshark executable name - Dump and analyze network traffic
        /// </summary>
        private const string s_Tshark = "tshark.exe";

        /// <summary>
        /// Quotation mark
        /// </summary>
        private const string s_Qm = "\"";

        /// <summary>
        /// Converts pcap file to csv file
        /// </summary>
        /// <param name="inputFilePath">Input file path</param>
        /// <returns>Output file path</returns>
        public static string ConvertPcapToCsvFile(string inputFilePath)
        {
            bool isPathValid = CheckInputFilePath(inputFilePath);

            if (!isPathValid)
            {
                return null;
            }
            return RunWireshark(inputFilePath, "");
        }

        /// <summary>
        /// Converts pcap file to csv file
        /// The filter is applied before writing packets to the csv file.
        /// The packets cannot pass the filter will not be written in the csv file.
        /// </summary>
        /// <param name="inputFilePath">Input file path</param>
        /// <param name="filter">Filter</param>
        /// <returns>Output file path</returns>
        public static string ConvertPcapToCsvFile(string inputFilePath, string filter)
        {
            bool isPathValid = CheckInputFilePath(inputFilePath);

            if (!isPathValid)
            {
                return null;
            }
            return RunWireshark(inputFilePath, filter);
        }

        private static bool CheckInputFilePath(string inputFilePath)
        {
            if (!File.Exists(inputFilePath))
            {
                // Log an error with an exception
                log.Error("File cannot be found: " + inputFilePath);
                return false;
            }
            return true;
        }

        /// <summary>
        /// This method executes following command:
        /// tshark -Y {filter} -r {input file} -w {output file}
        /// "C:\Program Files\Wireshark\tshark.exe"
        /// -Y "filter"
        /// -r "x.pcap"
        /// -w "x_csv.csv"
        /// </summary>
        /// <param name="inputFilePath">Input file path</param>
        /// <param name="filter">Filter</param>
        /// <returns>Output file path</returns>
        private static string RunWireshark(string inputFilePath, string filter)
        {
            //Control if Wireshark is installed
            bool isWiresharkInstalled = CheckTshark();

            if (!isWiresharkInstalled)
            {
                log.Error("Wireshark is not installed!");
                return null;
            }

            string outputFilePath = SetOutputPath(inputFilePath);

            log.Info("Following pcap file will be extracted as csv " + inputFilePath);

            string cmd = CreateCommand(inputFilePath, filter, outputFilePath);

            //Call regarding executable from CMD
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                WorkingDirectory = s_WiresharkDirectory,
                FileName = @"C:\Windows\System32\cmd.exe",
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                Arguments = cmd
            };
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            process.EnableRaisingEvents = true;
            process.Exited += (sender, e) => Completed(process.ExitCode);

            if (process.ExitCode != 0)
            {
                log.Error("Process cannot be completed!");
                return null;
            }

            return outputFilePath;
        }

        /// <summary>
        /// Checks if Wireshark is installed
        /// </summary>
        /// <returns></returns>
        private static bool CheckTshark()
        {
            bool retValue = false;
            string[] filePaths = Directory.GetFiles(s_WiresharkDirectory, "*.exe");

            foreach (var sFilePath in filePaths)
            {
                string sFileName = Path.GetFileName(sFilePath);
                if (sFileName.Contains("tshark"))
                {
                    retValue = true;
                }
            }

            if (!retValue)
            {
                log.Error("Wireshark is not installed at " + s_WiresharkDirectory);
            }

            return retValue;
        }

        /// <summary>
        /// Set output file path
        /// </summary>
        /// <param name="inputFilePath">Input file path</param>
        /// <returns>Output file path</returns>
        private static string SetOutputPath(string inputFilePath)
        {
            return Path.GetDirectoryName(inputFilePath) + "\\" +
                                    Path.GetFileNameWithoutExtension(inputFilePath) + "_auto.csv";
        }

        /// <summary>
        /// Creates appropriate input command for Tshark.
        /// This method creates following command:
        /// tshark -Y {filter} -r {input file} -w {output file}
        /// "C:\Program Files\Wireshark\tshark.exe"
        /// -Y "filter"
        /// -r "x.pcap"
        /// -w "x_csv.csv"
        /// </summary>
        /// <param name="inputFilePath">Input file name</param>
        /// <param name="filter">Filter</param>
        /// <param name="outputFilePath">Output file name</param>
        /// <returns></returns>
        private static string CreateCommand(string inputFilePath, string filter, string outputFilePath)
        {
            string cmd = "/C ";
            cmd += s_Tshark;
            cmd += " -T fields -E header=y -E separator=, -E quote=d " +
                   "-e _ws.col.No. -e _ws.col.Time -e _ws.col.Source -e _ws.col.Destination " +
                       "-e _ws.col.Protocol -e _ws.col.Length -e _ws.col.Info";
            cmd += " -Y " + s_Qm + filter + s_Qm;
            cmd += " -r " + s_Qm + Path.GetFullPath(inputFilePath) + s_Qm;
            cmd += " > " + s_Qm + Path.GetFullPath(outputFilePath) + s_Qm;
            return cmd;
        }

        /// <summary>
        /// When the process is completed, this method is called
        /// </summary>
        private static void Completed(int exitCode)
        {
            if(exitCode != 0)
            {
                log.Error("Conversion is not sucessfull! Exit code is " + exitCode);
            }
            else
            {
                log.Info("Conversion is sucessfull!");
            }
        }
    }
}
