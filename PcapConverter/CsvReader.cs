﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PcapConverter
{
    /// <summary>
    /// CsvReader reads csv files which are generated by PcapHandler.ConvertPcapToCsvFile methods. 
    /// </summary>
    public class CsvReader
    {
        /// <summary>
        /// Reads csv file and returns list of packets inside the csv file.
        /// </summary>
        /// <param name="csvFilePath">Input file path</param>
        /// <returns>List of csv packets</returns>
        public static List<CsvPacket> Read(string csvFilePath)
        {
            List<CsvPacket> csvPackets = new List<CsvPacket>();

            using (var reader = new StreamReader(csvFilePath))
            {
                reader.ReadLine();
                char[] delimiterChars = { '\"', '\"' };

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line == null) continue;
                    string[] words = line.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                    words = words.Where(w => w != ",").ToArray();
                    CsvPacket packetSummary = new CsvPacket
                    {
                        Number = words[0],
                        Time = words[1],
                        Src = words[2],
                        Dst = words[3],
                        Protocol = words[4],
                        Length = words[5],
                        Info = words[6]
                    };
                    csvPackets.Add(packetSummary);
                }
            }
            return csvPackets;
        }
    }
}
