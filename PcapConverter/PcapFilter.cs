﻿namespace PcapConverter
{
    /// <summary>
    /// This class holds filtering texts.
    /// There are lots of filter types exist.
    /// More detail can be found under following link:
    /// https://wiki.wireshark.org/DisplayFilters
    /// </summary>
    public static class PcapFilter
    {
        public const string s_Udp = "udp";
        public const string s_Pn_Dcp = "pn_dcp";
        public const string s_Arp = "arp";
    }
}
