# simple-pcap-converter

This is a simple C# class library for converting pcap files.

This tool wraps Wireshark's tshark command line tool for converting pcap files to csv files.
To use this tool, you need to install Wireshark.